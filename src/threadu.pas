unit threadu;

{$mode delphi}

interface

uses
  Classes, SysUtils, Forms, Global, Masks;

type
    TScanColpleteEvent =Procedure Of Object;
    TShowLogEvent = procedure(LogStatus:TLogStatus; Filename:String) of Object;
    TVirusFoundEvent = procedure(VirusName,VirusFile:string) of Object;
    TShowStatusEvent = procedure(Precent: Integer ; FileName:String) of Object; //евент для процентов\
   // TWarningEvent = procedure(Precent: Integer ; FileName:String) of Object;
    TLoadDBEvent= procedure(Signatures:Integer) of Object; //LoadingDb Ok events
    TErrorDBEvent = procedure of Object;
    TCoutEvent = procedure(Count:Integer) of Object;//
type
  { TScanThread }
    TScanThread = class(TThread)
    private
      FOnScanComplete: TScanColpleteEvent;
      FOnShowLog: TShowLogEvent;
      FOnVirusFound: TVirusFoundEvent;
      FVirusName,FVirusFile:String;
      fPrecent : integer;
      fScanName:String;
      FOnShowStatus: TShowStatusEvent;
      FLogStatus:TLogStatus;
      fFilename:string;
      procedure ScanStatus;
      procedure ScanLog;
      procedure SkipFiles;
      procedure VirusFound;
      procedure ScanComplete;
      procedure ScanFiles(StartFolder, Mask: string; ScanSubFolders: Boolean = True);
      procedure Return;
    protected
      procedure Execute; override;
    public
      Constructor Create(CreateSuspended : boolean);
      property OnScanStatus: TShowStatusEvent read FOnShowStatus write FOnShowStatus;
      property OnScanLog: TShowLogEvent read FOnShowLog write FOnShowLog;
      property OnVirusFound: TVirusFoundEvent read FOnVirusFound write FOnVirusFound;
      property OnScanComplete:TScanColpleteEvent read FOnScanComplete write FOnScanComplete;
    end;
  { TLoadDBThread }
    TLoadDBThread =class(TThread)
    private
      FErrorDb: TErrorDBEvent;
      FLoadDB: TLoadDBEvent;
      FSigsnature:integer;
      procedure LoadDb;
      procedure ErrorDb;
     protected
      procedure Execute; override;
    public
      Constructor Create(CreateSuspended : boolean);
      property OnLoadDB: TLoadDBEvent read FLoadDB write FLoadDB;
      property OnErrorDb: TErrorDBEvent read FErrorDb write FErrorDb;
    end;

    TFilesComleteEvent = procedure of Object;
    { TFilesThread }
    TFilesThread = class(TThread)
     private
       FComplete: TFilesComleteEvent;
       FCount: TCoutEvent;
       FCountTotal:integer;
       procedure Complete;
       procedure Count;
    protected
      procedure Execute; override;
      procedure FindFiles(StartFolder, Mask: string; {List: TStrings;} ScanSubFolders: Boolean = True);

    public
      Constructor Create(CreateSuspended : boolean);
      property OnComlete:TFilesComleteEvent read FComplete write FComplete;
      property OnCount:TCoutEvent read FCount write FCount;
    end;

// TCoutEvent = procedure(Count:Integer) of Object;//

implementation
 uses clamav3;
{ TScanThread }
procedure TScanThread.ScanStatus;
// этот метод запущен главным потоком и поэтому может получить доступ ко всем элементам графического интерфейса.
begin
  if Assigned(FOnShowStatus) then
    begin
      FOnShowStatus(fPrecent,fScanName);
    end;
end;

procedure TScanThread.ScanLog;
begin
  if Assigned(FOnShowLog) then
    begin
      FOnShowLog(FLogStatus,fFilename);
    end;
end;

procedure TScanThread.SkipFiles;
begin

end;

procedure TScanThread.VirusFound;
begin
  if Assigned(FOnVirusFound) then
    begin
      FOnVirusFound(FVirusName,FVirusFile);
    end;
end;

procedure TScanThread.ScanComplete;
begin
 if Assigned(FonScanComplete) then FonScanComplete;
end;

procedure TScanThread.ScanFiles(StartFolder, Mask: string;
  ScanSubFolders: Boolean);
Var
    scanned    : Word;

{------------------------------------------------------------------------------}
Function __ScanFile(const xfilex: String):longint;
     begin
       try
  {$Warnings Off}
  Result := cl_scanfile(PChar(xfilex), @virname, scanned, cl_engine(Engine^), CL_SCAN_STDOPT);
  {$Warnings On}
       except
       end;
     end;
{------------------------------------------------------------------------------}
var
SearchRec: TSearchRec;
FindResult: Integer;
begin
 If DirectoryExists(StartFolder) then
try
StartFolder := IncludeTrailingBackslash(StartFolder);
FindResult := FindFirst(StartFolder + '*', faNormal-faDirectory, SearchRec);
try
  while FindResult = 0 do
    with SearchRec do
    begin
      if (Attr and faDirectory) <> 0 then
      begin
        if ScanSubFolders and (Name <> '.') and (Name <> '..') then
          ScanFiles(StartFolder + Name, Mask, ScanSubFolders);
      end
      else
      begin
        if MatchesMask(Name, Mask) then
          Begin
          Inc(fPrecent);
          fScanName:= StartFolder + Name;
          Synchronize(ScanStatus);
          ret :=__ScanFile(fScanName);
          Return;
            If Stopping=true then
              begin
                FindClose(SearchRec);
              end;

          end;
      end;
       If Stopping=true then FindClose(SearchRec);
      FindResult := FindNext(SearchRec);
    end;
finally
  FindClose(SearchRec);
end;
finally
end else
    begin
     Inc(fPrecent);
     fScanName:= StartFolder;
     Synchronize(ScanStatus);
     ret :=__ScanFile(fScanName);
     Return
   end;
//------------------------------------------------------------------------------
end;

procedure TScanThread.Return;
begin
     case ret of
  CL_CLEAN or CL_SUCCESS:
    begin
     someresult := AnsiString(cl_strerror(ret));
     inc(CountFile);
     //fFilename:='['+(fScanName)+'] '+someresult;
     // FLogStatus:=LsWarning;
     // Synchronize(ScanLog);
    end;
  CL_VIRUS:
    begin
         someresult:= AnsiString(virname);
         inc(CountFile);
         fFilename:='['+ExtractFileName(fScanName)+'] '+someresult;
         FLogStatus:=LsVirus;
         Synchronize(ScanLog);
         FVirusName:=someresult;
         FVirusFile:=fScanName;
         Synchronize(VirusFound);
    end;
  CL_EOPEN,CL_ECREAT,CL_EUNLINK,CL_ESTAT,CL_EREAD,CL_ESEEK,CL_EWRITE,
  CL_EDUP,CL_EACCES,CL_ETMPFILE,CL_ETMPDIR,CL_EMAP,CL_EMEM,CL_ETIMEOUT:
    begin
      Inc(SkipFile);
      someresult := AnsiString(cl_strerror(ret));
      fFilename:='['+ExtractFileName(fScanName)+'] '+someresult;
      FLogStatus:=LsWarning;
      Synchronize(ScanLog);
     end;
  end;
end;

procedure TScanThread.Execute;
var
  i: integer;
  begin
    fPrecent := 0;
    if (not Terminated) and isDBLoaded then
      begin
        for i:=0 to ScanList.Count-1 do
       // For i:=1 to Application.ParamCount do
      begin
       ScanFiles(ScanList.Strings[i],'*',True);
      end;

      end;
       Synchronize(ScanComplete);
       Terminate;
      end;


constructor TScanThread.Create(CreateSuspended: boolean);
begin
    FreeOnTerminate := True;
    inherited Create(CreateSuspended);
end;

{ TFilesThread }
procedure TFilesThread.Complete;
begin
if Assigned(FComplete) then FComplete;
end;

procedure TFilesThread.Count;
begin
  if Assigned(FCount) then FCount(FCountTotal);
end;

procedure TFilesThread.Execute;
var i:integer;
begin
 for i:=0 to ScanList.Count-1 do
begin
if DirectoryExists(ScanList.Strings[i]) then FindFiles(ScanList.Strings[i],'*',{scanlist,}true)
else
  begin
  inc(FCountTotal);
  Synchronize(Count);
  end;
end;
    Synchronize(FComplete);
    Terminate;
end;

procedure TFilesThread.FindFiles(StartFolder, Mask: string; ScanSubFolders: Boolean);
var
SearchRec: TSearchRec;
FindResult: Integer;
begin
//
try
StartFolder := IncludeTrailingBackslash(StartFolder);
FindResult := FindFirst(StartFolder + '*', faNormal-faDirectory, SearchRec);
try
  while FindResult = 0 do
    with SearchRec do
    begin
      if (Attr and faDirectory) <> 0 then
      begin
        if ScanSubFolders and (Name <> '.') and (Name <> '..') then
          FindFiles(StartFolder + Name, Mask, ScanSubFolders);
      end
      else
      begin
        if MatchesMask(Name, Mask) then
        begin
          inc(FCountTotal);
          Synchronize(Count);
          if Stopping then  FindClose(SearchRec);
        end;
      end;
       if Stopping then  FindClose(SearchRec);
      FindResult := FindNext(SearchRec);
    end;
finally
  FindClose(SearchRec);
end;
finally
//
end;
end;

constructor TFilesThread.Create(CreateSuspended: boolean);
begin
FreeOnTerminate := True;
inherited Create(CreateSuspended);
end;

{ TLoadDBThread }
procedure TLoadDBThread.LoadDb;
begin
if Assigned(FLoadDB) then FLoadDb(FSigsnature);
end;

procedure TLoadDBThread.ErrorDb;
begin
if Assigned(FErrorDb) then FErrorDb;
end;

procedure TLoadDBThread.Execute;
var okret : integer;
path:string;
begin

  {$IFDEF Windows}
// change the path here...
Path := 'database';
{$ENDIF}
 {$IFDEF Linux}
//for linux return db directory
Path := cl_retdbdir;

{$ENDIF}
  //---------------------------------------
try
  if cl_load(PChar(path),cl_engine(Engine^),sigs,CL_DB_OFFICIAL)=CL_SUCCESS then
    //---------------------------------------
    try
       okret :=cl_engine_compile(cl_engine(Engine^));
      isDBLoaded := True;
      FSigsnature:=sigs;
      Synchronize(LoadDB);
      Terminate;
    except
     on E : Exception do
     begin
        // Log(LsWarning,'E : '+E.Message);
        // Log(LsWarning,'Unable to LoadDB '+AnsiString(cl_strerror(okret)));
         { TODO : Обрабочик ошибок базы }
         cl_engine_free(cl_engine(Engine^));
         Synchronize(ErrorDB);
      Terminate;
     end;
    end;
    //---------------------------------------
except
  on E : Exception do
  begin
   // Log(LsWarning,'Unable to LoadDB '+E.Message);
    cl_engine_free(cl_engine(Engine^));
    Synchronize(ErrorDB);
      Terminate;
  end;
end;
end;
//---------------------------------------------

constructor TLoadDBThread.Create(CreateSuspended: boolean);
begin
FreeOnTerminate := True;
inherited Create(CreateSuspended);
end;




end.

