unit mUnit;


{$mode delphi}{$H+}
interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  StdCtrls, CBColorButton, CBDropLabel, slider, clamAv3, global,
  threadu;
//ClamBit scanner for detecting trojans, viruses, malware and other malicious threats.
type
  { TMainForm }
  TMainForm = class(TForm)
   Image1: TImage;
   ErrorPage: TPage;
   Label3: TLabel;
   Memo1: TMemo;
   ScanBar: TCBSlider;
   Label1: TLabel;
   CancelScanBtn: TCBColorButton;
   ScanPage: TPage;
   ScanCustomBtn: TCBColorButton;
    ScanHintLbl: TCBDropLabel;
    ScanHomeBtn: TCBColorButton;
    LogoImg: TImage;
    Logolbl: TLabel;
    ScanAllFilesBtn: TCBColorButton;
    SelectionPage: TPage;
    PageCtrl: TNotebook;
    AniTimer: TTimer;
    procedure CancelScanBtnClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure HintEvent(Sender: TObject);
    procedure ScaningClick(Sender : TObject);
    procedure ScaningClickRun(Sender: TObject);
    procedure InitEngine;
    procedure AniTimerTimer(Sender: TObject);
 private
    LoadDBThread : TLoadDBThread; // поток для загрузки базы вирусов
    FilesThread: TFilesThread; // поток для количества сканируемых файлов
    ScanThread: TScanThread; // поток для сканиррования
   {LoadDbThread}
   procedure OnLoadDB(Signatures:Integer); // событие загрузки базы
   {FilesThread}
   procedure FilesCount(Count:Integer);// количество файлов
   procedure ComletePreSearch;// закончили поиск
   {ScanThread}
   procedure VirusFound(VirusName,VirusFile:string); // найден вирус
   procedure ShowStatus(Precent: Integer ; FileName:String);// Статус сканирования
   procedure ScanComplete;// закончили сканирование
   procedure ScanLog(LogStatus:TLogStatus; Filename:String);
 public
    { public declarations }
  end;

var
  MainForm: TMainForm;
  VirCount:Integer;
  CountFiles:Integer;
implementation
//uses LazGlib2;
{$R *.lfm}

{ TMainForm }
procedure TMainForm.HintEvent(Sender: TObject); { Подсказки для гоавного меню }
begin // Hint string of main page
 if (Sender is TPage) or (Sender is TImage) or (Sender is TLabel) then ScanHintLbl.DropText:='';
 if Sender is TCBColorButton then
 ScanHintLbl.DropText:=(Sender as TCBColorButton).Hint;
 ScanHintLbl.DropCaption;
end;

procedure TMainForm.FormCreate(Sender: TObject);
begin
  LoadDBThread:=TLoadDBThread.Create(true);
  LoadDBThread.OnLoadDB:=OnLoadDB;
  InitEngine;
end;

procedure TMainForm.CancelScanBtnClick(Sender: TObject); { Отмена сканированния }
begin
 Stopping:=true;
if Assigned(ScanThread) then ScanThread:=nil;
If Assigned(FilesThread) then FilesThread:=nil;
 PageCtrl.PageIndex:=0;
end;

procedure TMainForm.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
   Stopping:=true;
   if Assigned(ScanThread) then ScanThread:=nil;
   if Assigned(FilesThread) then FilesThread:=nil;
   if isDBLoaded then cl_engine_free(cl_engine(Engine^));
   if Assigned(LoadDBThread) then LoadDBThread:=nil;
   if Assigned(Engine) then Engine := nil;
end;

procedure TMainForm.ScaningClick(Sender: TObject); { Для всех кнопок старт анимации }
begin
   (Sender as TCBColorButton).Animation;
end;

procedure TMainForm.ScaningClickRun(Sender: TObject); { Запуск сканирования }
begin
   Stopping:=false;   ScanList.Clear;
   VirCount:=0;   CountFiles:=0;
   ScanBar.SetParams(0,0,0);

  if not assigned(FilesThread) then
  begin
   FilesThread:=TFilesThread.Create(true);
   FilesThread.OnCount:=FilesCount;
   FilesThread.OnComlete:=ComletePreSearch;
  end;

  if not assigned(ScanThread) then
  begin
   ScanThread := TScanThread.Create(true);
   ScanThread.OnScanStatus := ShowStatus;
   ScanTHread.OnVirusFound:=VirusFound;
   ScanTHread.OnScanComplete:=ScanComplete;
   ScanTHread.OnScanLog:=ScanLog;
  end ;

 if Sender is TCBColorButton then
  Case (Sender as TCBColorButton).Tag of // проверка по свойству .Tag
  { 0 - домашняя папка
  1 - системные файлы
  2- выборочно }
  0: begin
    ScanList.Add(GetUserDir);
    PageCtrl.PageIndex:=1;
   {$Warnings Off}
    FilesThread.Resume;
   if isDBLoaded then ScanThread.Resume;
   {$Warnings On}
    end;

  1: begin
    ScanList.Add('/media/vadim/5024319324317D52/Program Files (x86)/360/');
    PageCtrl.PageIndex:=1;
  {$Warnings Off}
    FilesThread.Resume;
   if isDBLoaded then ScanThread.Resume;
   {$Warnings On}
      end;

  end;//case

end;

procedure TMainForm.InitEngine; { Инициализация сканнера }
begin // Инициализация сканнера ===
  if (cl_init(CL_INIT_DEFAULT)=CL_SUCCESS) then
  begin
    Engine := cl_engine_new();// создали новый
    {$Warnings Off}
    LoadDBThread.Resume; // запустили загрузку базы данных
    {$Warnings On}
  end else
  begin // если не пошло то выходим
    Application.Terminate;
  end;
end;

procedure TMainForm.AniTimerTimer(Sender: TObject); { Костыль для анимации - чтоб строка не дерганная была }
begin
 if ScanHintLbl.Caption='' then ScanHintLbl.DropCaption;
end;



procedure TMainForm.OnLoadDB(Signatures: Integer); {Событие загрузки базы}
begin
 ScanBar.Caption:=' ';//Loaded signatures '+IntToStr(Signatures);
 if Assigned(ScanThread) then {$Warnings Off} ScanThread.Resume {$Warnings On}
end;

procedure TMainForm.FilesCount(Count: Integer);  { счетчик количества файлов }
begin
 ScanBar.SetParams(0,Count,ScanBar.Position);
 CountFiles:=Count;
 ScanBar.Caption:=Inttostr(CountFiles);
end;

procedure TMainForm.ComletePreSearch; { Закончили накручивать счет файлов для бара }
begin
 FilesThread:=nil;
end;

procedure TMainForm.VirusFound(VirusName, VirusFile: string); {Нашли бяку}
begin
  Inc(VirCount);
end;

procedure TMainForm.ShowStatus(Precent: Integer; FileName: String);{Статус Сканнировки}
begin
 //ScanBar.Position:=Precent;
 ScanBar.SetParams(0,CountFiles,Precent);
 Label1.Caption:='ScanFiles '+Inttostr(Precent)+' Treads '+ inttostr(VirCount);
end;

procedure TMainForm.ScanComplete;{ Сканнирование завершено }
begin
  ScanThread:=nil;
  PageCtrl.PageIndex:=0;
end;

procedure TMainForm.ScanLog(LogStatus: TLogStatus; Filename: String);
begin
 memo1.Lines.Add(Filename);
end;

end.

