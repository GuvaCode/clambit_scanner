program clambitscan;

{$mode objfpc}{$H+}


uses
  {$IFDEF UNIX}
  cthreads,cmem,
  {$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms, mUnit, global;

{$R *.res}

begin
  Application.Title:='ClamBit Scanner';
  RequireDerivedFormResource:=True;
  Application.Initialize;
  Application.CreateForm(TMainForm, MainForm);
  Application.Run;
end.

