unit global;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils;
type
  TLogStatus = (LsNone, LsInfo, LsWarning, LsError, LsComplete, LsVirus , LsDb, LsEngine);

Var
  ScanList     : TStringList;
  ret          : Integer;
  sigs         : longint;
  isDBLoaded   : Boolean = false;
  VirusFound   : Integer = 0;
  SkipFile     : Integer = 0;
  CountFile    : Integer = 0;
  someresult   : String;
  virname      : PAnsiChar;
  ShowProgress : Boolean=False;
  Stopping     : Boolean=False;
  Complete     : Boolean=False;
  ScanHiden    : Boolean=False;
  ScanFullUser : Boolean=False;

resourcestring
  rsScipFile        ='Пропущено файлов';
  rsScanCount       ='Проверено файлов';
  rsVirusCount      ='Обнаружено угроз';

const
  uDownload ='download';


implementation



initialization
If not Assigned(ScanList) then ScanList:=TStringList.Create;

finalization
 If Assigned(ScanList) then ScanList.Free;

end.

