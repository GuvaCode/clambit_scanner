unit CBColorButton;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, LResources, Forms, Controls, Graphics, Dialogs, LCLType,
  LCLIntf, Messages, uanimationcontrol, uanimationbasic, extctrls;


type
  TAnimationEnd=procedure (Sender: TObject) of object;
//procedure (Sender: TObject);

type
  { TCBColorButton }
  TCBColorButton = class(TGraphicControl)
  private
    FAnimationEnd: TAnimationEnd;

    FColorDisabled: TColor;
    FColorFocused: TColor;
    FColorNormal: TColor;
    FColorPressed: TColor;
    FEnabled:Boolean;

    FPressed: Boolean;
    FFocused: Boolean;
    FDrawing: Boolean;
    AnimationQueue: TAnimationQueue;
    FTransitionMode: TAnimationTransitionMode;
    FZoomEfect: TAnimationZoomMode;
    AnimationTimer:TTimer;

    procedure SetColorDisabled(AValue: TColor);
    procedure SetColorFocused(AValue: TColor);
    procedure SetColorNormal(AValue: TColor);
    procedure SetColorPressed(AValue: TColor);
    procedure CMMouseEnter(var Message: TMessage); message CM_MOUSEENTER;
    procedure CMMouseLeave(var Message: TMessage); message CM_MOUSELEAVE;
    procedure OnColorChange(Sender: TObject);
    procedure SetTransitionMode(AValue: TAnimationTransitionMode);
    procedure SetZoomEfect(AValue: TAnimationZoomMode);
    procedure UpdateButtonState;
    procedure AniTimer(Sender: TObject);
    Procedure AnimationEnd(Sender: TAnimationItem);
    { Private declarations }
  protected
   procedure Paint; override;
   procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
   procedure MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
   procedure MouseEnter; override;

  public
   constructor Create(AOwner: TComponent); override;
   destructor Destroy; override;
   procedure Animation;
   procedure AnimationCaption;


  published
   property Align;
   property Action;
   property Anchors;
   property Caption;
   property Constraints;
   property BorderSpacing;
   property Enabled;
   property Font;
   property ShowHint;
   property ParentShowHint;
   property OnClick;
   property OnMouseDown;
   property OnMouseEnter;
   property OnMouseLeave;

   property OnAnimationEnd:TAnimationEnd read FAnimationEnd write FAnimationEnd;
   property ColorNormal:TColor read FColorNormal write SetColorNormal;
   property ColorFocused:TColor read FColorFocused write SetColorFocused;
   property ColorPressed:TColor read FColorPressed write SetColorPressed;
   property ColorDisabled:TColor read FColorDisabled write SetColorDisabled;
   property ZoomEfect:TAnimationZoomMode read FZoomEfect write SetZoomEfect;
   property TransitionMode:TAnimationTransitionMode read FTransitionMode write SetTransitionMode;
  end;

procedure Register;

implementation

procedure Register;
begin
  {$I cbcolorbutton_icon.lrs}
  RegisterComponents('ClamBit',[TCBColorButton]);
end;

{ TCBColorButton }

procedure TCBColorButton.SetColorDisabled(AValue: TColor);
begin
  if FColorDisabled=AValue then Exit;
  FColorDisabled:=AValue;
end;





procedure TCBColorButton.SetColorFocused(AValue: TColor);
begin
  if FColorFocused=AValue then Exit;
  FColorFocused:=AValue;
end;

procedure TCBColorButton.SetColorNormal(AValue: TColor);
begin
  if FColorNormal=AValue then Exit;
  FColorNormal:=AValue;
end;

procedure TCBColorButton.SetColorPressed(AValue: TColor);
begin
  if FColorPressed=AValue then Exit;
  FColorPressed:=AValue;
end;

procedure TCBColorButton.Paint;
var
R: TRect;
begin
  inherited Paint;
  if FDrawing then Exit;

FDrawing := True;
try
   UpdateButtonState;
   if Caption <> '' then
   begin
     R := ClientRect;
     Canvas.Font.Assign(Font);
     Canvas.Brush.Style := bsClear;

     R := ClientRect;
     R.Top := 0;
     R.Bottom := 0;
     Inc(R.Left, 14);
     Dec(R.Right, 14);
     DrawText(Canvas.Handle, PChar(Caption), -1, R, DT_WORDBREAK or DT_CALCRECT);

     R.Right := ClientWidth - 14;
     R.Top := (ClientHeight - (R.Bottom - R.Top)) div 2;
     R.Bottom := ClientHeight;
    with Canvas do
   begin
     Pen.Style := psSolid;
     Pen.Color := Color;//clBlack;
     Brush.Color := Color;
     Brush.Style := bsSolid;
    // Rectangle(0, 0, Width, Height);
     RoundRect(0, 0, ClientWidth, ClientHeight,4,2);
   end;
     DrawText(Canvas.Handle, PChar(Caption), -1, R, DT_WORDBREAK or DT_CENTER);
   end;
finally
   FDrawing := False;
end;
end;

procedure TCBColorButton.MouseDown(Button: TMouseButton; Shift: TShiftState; X,
  Y: Integer);
begin
  inherited MouseDown(Button, Shift, X, Y);
  if Enabled = False then Exit;
  if Button = mbLeft then
   begin
    FPressed := True;
    FFocused := True;
    if not FDrawing then Invalidate;
   end;
end;

procedure TCBColorButton.MouseUp(Button: TMouseButton; Shift: TShiftState; X,
  Y: Integer);
begin
  if Enabled = False then Exit;
  if Button = mbLeft then
 begin
   FPressed := False;
   if not FDrawing then Invalidate;
 end;
  inherited MouseUp(Button, Shift, X, Y);
end;

procedure TCBColorButton.MouseEnter;
begin
  if not FDrawing then Invalidate;
inherited MouseEnter;

end;

procedure TCBColorButton.AnimationEnd(Sender: TAnimationItem);
begin
  if assigned(FAnimationEnd) then FAnimationEnd(Self);
end;

constructor TCBColorButton.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
   //FPictureNormal.OnChange := OnPictureChange;
  FEnabled := True;
  FcolorNormal:=$00D2782D;
  FColorFocused :=$00DB9357;
  FColorPressed :=$00D2782D;
  FColorDisabled :=ClSilver;
  FPressed := False;
  FFocused := False;
  FDrawing := False;
  Width:=130;
  Height:=40;
  AnimationTimer:=TTimer.Create(self);
  AnimationTimer.Interval:=15;
  AnimationTimer.OnTimer:=@AniTimer;
  AnimationTimer.Enabled:=true;
  AnimationQueue:=TAnimationQueue.Create;
  AnimationQueue.Start(true);

end;

destructor TCBColorButton.Destroy;
begin
  FreeAndNil(AnimationTimer);
  FreeAndNil(AnimationQueue);
  inherited Destroy;
end;

procedure TCBColorButton.Animation;
var
butAnim: TAnimationControlZoom;
begin
   AnimationQueue.RemoveGroupID(self);
  butAnim:=TAnimationControlZoom.Create(self);
  butAnim.SetFinalZoom(0.25);
  butAnim.ZoomMode:=FZoomEfect;
  butAnim.TransitionMode:=FTransitionMode;
  butAnim.AutoReverse:=true;
  butAnim.Repeats:=2;
  butAnim.AnimationOnEndAction:=TAnimationOnEndAction.eAnimationOnEndFree;
  butAnim.Duration:=100;
  butAnim.GroupID:=self;
  AnimationQueue.Add(butAnim,true);
  butAnim.OnAnimationEnd:=@AnimationEnd;
  butAnim.Start;
end;

procedure TCBColorButton.AnimationCaption;
var
  Ef: TAnimationControlCaptionCapital;
begin
  AnimationQueue.RemoveGroupID(self);
  Ef:=TAnimationControlCaptionCapital.Create(self);
  {$PUSH}{$HINTS OFF} // Hide hint about use int64
  Ef.Duration:=1000;//5*Length(Caption);
  {$POP}
  ef.Repeats:=20;
  ef.AutoReverse:=true;
  ef.TransitionMode:=eAnimationTransitionBallisticBoth;
  EF.AnimationOnEndAction:=eAnimationOnEndFree;
  AnimationQueue.Add(Ef);
  EF.Start;
end;



procedure TCBColorButton.CMMouseEnter(var Message: TMessage);
begin
 if Enabled = False then Exit;
 FFocused := True;
 if not FDrawing then Invalidate;
 If (FFocused) and Assigned(OnMouseEnter) then OnMouseEnter(Self);
end;

procedure TCBColorButton.CMMouseLeave(var Message: TMessage);
begin
 if Enabled = False then Exit;
 FFocused := False;
 if not FDrawing then Invalidate;
// if not FFocused OnMouseLeave(Self);
end;

procedure TCBColorButton.OnColorChange(Sender: TObject);
begin

end;

procedure TCBColorButton.SetTransitionMode(AValue: TAnimationTransitionMode);
begin
  if FTransitionMode=AValue then Exit;
  FTransitionMode:=AValue;
end;

procedure TCBColorButton.SetZoomEfect(AValue: TAnimationZoomMode);
begin
  if FZoomEfect=AValue then Exit;
  FZoomEfect:=AValue;
end;

procedure TCBColorButton.UpdateButtonState;
begin
 if Enabled then
begin
   if not (csDesigning in ComponentState) then
   begin
     if (FPressed and FFocused) then
       Color:=ColorPressed
     else
       if (not FPressed and FFocused) then
         Color := ColorFocused
       else
         Color := ColorNormal;
   end
         else Color := ColorNormal;
 end
  else
  begin
   FFocused := False;
   FPressed := False;
   Color := ColorDisabled;
  end;


  if ColorNormal <> ColorNormal  then
   Color := ColorNormal;

if csDesigning in ComponentState then
   // ((not Assigned(Picture.Graphic)) or (Picture.Width = 0) or (Picture.Height = 0)) then
begin
   with Canvas do
   begin
     Pen.Style := psDash;
     Pen.Color := clBlack;
     Brush.Color := Color;
     Brush.Style := bsSolid;
    // Rectangle(0, 0, Width, Height);
     RoundRect(0, 0, ClientWidth, ClientHeight,4,2);
   end;

   Exit;
end;

end;

procedure TCBColorButton.AniTimer(Sender: TObject);
begin
//Self.DisableAutoSizing;
// Self.DisableAlign;
  AnimationQueue.Animate;

  //if  then click;
 // Self.EnableAlign;
// Self.EnableAutoSizing;
  // Adding Self.Invalidate improves smooth animation but can make
  // the form less responsive in low power machines.
  // When used removes some animation artifacts in controls translation.
  //Self.Invalidate;
 // Caption:=format('FPS: %.2f',[AnimationQueue.AverageFPS]);
end;




end.
