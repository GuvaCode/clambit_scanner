unit cb_register;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, LResources, CBnotify;

Procedure Register;

implementation

procedure Register;
begin
  {$I ../res/notify_icon.lrs}
  RegisterComponents('ClamBit',[TCBNotify]);
end;


end.

