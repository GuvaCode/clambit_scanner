unit CBDropLabel;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, LResources, Forms, Controls, Graphics, Dialogs, StdCtrls,
  uanimationcontrol, uanimationbasic, extctrls;


type
  { TCBDropLabel }
  TCBDropLabel = class(TLabel)
  private
    AnimationQueue: TAnimationQueue;
    AnimationTimer:TTimer;
    FAnimationEnd: TAnimationOnAnimationEnd;
    FDropText: String;
    FDuration: int64;
    FTransitionMode: TAnimationTransitionMode;
    FBusy:boolean;
    procedure AniTimer(Sender: TObject);
    procedure SetDropText(AValue: String);
    procedure SetDuration(AValue: int64);
    procedure SetTransitionMode(AValue: TAnimationTransitionMode);
    Procedure AnimationEnd(Sender: TAnimationItem);
  protected
    { Protected declarations }
  public
   constructor Create(AOwner: TComponent); override;
   destructor Destroy; override;
   procedure DropCaption;
  published
   property DropText: String read FDropText write SetDropText;
   property Duration: int64 read FDuration write SetDuration;
   property TransitionMode:TAnimationTransitionMode read FTransitionMode write SetTransitionMode;

  end;

procedure Register;

implementation

procedure Register;
begin
  {$I cbdroplabel_icon.lrs}
  RegisterComponents('ClamBit',[TCBDropLabel]);
end;

{ TCBDropLabel }

procedure TCBDropLabel.AniTimer(Sender: TObject);
begin
   AnimationQueue.Animate;
end;

procedure TCBDropLabel.SetDropText(AValue: String);
begin
  if FDropText=AValue then Exit;
  FDropText:=AValue;
end;

procedure TCBDropLabel.SetDuration(AValue: int64);
begin
  if FDuration=AValue then Exit;
  FDuration:=AValue;
end;

procedure TCBDropLabel.SetTransitionMode(AValue: TAnimationTransitionMode);
begin
  if FTransitionMode=AValue then Exit;
  FTransitionMode:=AValue;
end;

procedure TCBDropLabel.AnimationEnd(Sender: TAnimationItem);
begin
  FBusy:=False;
end;

constructor TCBDropLabel.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  AnimationTimer:=TTimer.Create(self);
  AnimationTimer.Interval:=15;
  AnimationTimer.OnTimer:=@AniTimer;
  AnimationTimer.Enabled:=true;
  AnimationQueue:=TAnimationQueue.Create;
  AnimationQueue.Start(true);
  Duration:=600;
  FBusy:=false;
end;

destructor TCBDropLabel.Destroy;
begin
  FreeAndNil(AnimationTimer);
  FreeAndNil(AnimationQueue);
  inherited Destroy;
end;

procedure TCBDropLabel.DropCaption;
Var lDropText: TAnimationControlCaptionReplace;
begin
      If FDropText=Caption then exit;
      If FBusy then exit;
      lDropText:=TAnimationControlCaptionReplace.Create(self);
      lDropText.AnimationOnEndAction:=TAnimationOnEndAction.eAnimationOnEndFree;
      lDropText.Duration:=FDuration;
      lDropText.Repeats:=1;
      lDropText.TransitionMode:=FTransitionMode;
      lDropText.FinalText:=FDropText;
      AnimationQueue.Add(lDropText);
      lDropText.OnAnimationEnd:=@AnimationEnd;
      FBusy:=true;
      lDropText.Start;
end;

end.
