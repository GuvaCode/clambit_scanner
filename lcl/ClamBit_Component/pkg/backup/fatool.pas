unit fatool;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,
  fpimage, LCLType, IntfGraphics, GraphType,Graphics,
  EasyLazFreeType,  LazFreeTypeIntfDrawer;

function CreateFAIcon(constref ACode: String;{$IFDEF Linux} AFontAwesomePath:String;{$ENDIF} AColor :TColor ; ASize: Byte=16  ): TBitmap;

implementation

function CreateFAIcon(constref ACode: String; {$IFDEF Linux} AFontAwesomePath:String;{$ENDIF}
  AColor: TColor; ASize: Byte): TBitmap;
var
  img: TLazIntfImage;
  d: TIntfFreeTypeDrawer;
  f: TFreeTypeFont;
begin
  Result := TBitmap.Create;
  img := TLazIntfImage.Create(0,0, [riqfRGB, riqfAlpha]);
  d := TIntfFreeTypeDrawer.Create(img);
  f := TFreeTypeFont.Create;
  try
    {$IFDEF Linux}
    f.Name := AFontAwesomePath;
    {$ENDIF}
     {$IFDEF Windows}
    f.Name :='fontAwesome.ttf';
    {$ENDIF}
    f.SizeInPixels := Asize-2;
    f.Hinted := true;
    f.ClearType := true;
    f.Quality := grqHighQuality;
    f.SmallLinePadding := false;
    f.Style;
    img.SetSize(ASize,ASize);
    d.FillPixels(colTransparent);

    d.DrawText(ACode, f, ASize,ASize, TColorToFPColor(AColor), [ftaRight, ftaBottom]);

    Result.LoadFromIntfImage(img);
  finally
    f.Free;
    d.Free;
    img.Free;
  end;
end;

end.

