function CreateFAIcon(constref ACode: String): TBitmap;
var
  img: TLazIntfImage;
  d: TIntfFreeTypeDrawer;
  f: TFreeTypeFont;
begin
  Result := TBitmap.Create;
  img := TLazIntfImage.Create(0,0, [riqfRGB, riqfAlpha]);
  d := TIntfFreeTypeDrawer.Create(img);
  f := TFreeTypeFont.Create;
  try
    f.Name :=FFontAesomewPath;
    f.SizeInPixels := FFontAwesomeSize-2;
    f.Hinted := true;
    f.ClearType := true;
    f.Quality := grqHighQuality;
    f.SmallLinePadding := false;
    f.Style;
    img.SetSize(FFontAwesomeSize,FFontAwesomeSize);
    d.FillPixels(colTransparent);

    d.DrawText(ACode, f, FFontAwesomeSize, FFontAwesomeSize, TColorToFPColor(FFontAwesomeColor), [ftaRight, ftaBottom]);

    Result.LoadFromIntfImage(img);
  finally
    f.Free;
    d.Free;
    img.Free;
  end;
end;

