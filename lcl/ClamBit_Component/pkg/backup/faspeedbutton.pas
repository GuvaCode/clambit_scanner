unit FASpeedButton;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, LResources, Forms, Controls, Graphics, Dialogs, Buttons,
  fpimage, LCLType;

type

  { TFASpeedButton }

  TFASpeedButton = class(TSpeedButton)
  private
    FFontAesomewPath: String;
    FFontAwesomeColor: TColor;
    FFontAwesomeIcon: String;
    FFontAwesomeSize: Integer;
    procedure SetFontAwesomeColor(AValue: TColor);
    procedure SetFontAwesomeIcon(AValue: String);
    procedure SetFontAwesomePath(AValue: String);
    procedure SetFontAwesomeSize(AValue: Integer);
    { Private declarations }
  protected

     procedure SetIconFa;
  public
   constructor Create(AOwner: TComponent); override;

  published
   property FontAwesomeSize:Integer read FFontAwesomeSize write SetFontAwesomeSize;
   {$IFDEF Linux}
   property FontAwesomePath:String read FFontAesomewPath write SetFontAwesomePath;
   {$ENDIF}
   property FontAwesomeColor:TColor read FFontAwesomeColor write SetFontAwesomeColor;
   property FontAwesomeIcon:String read FFontAwesomeIcon write SetFontAwesomeIcon;
  end;

procedure Register;

implementation
uses fatool;
procedure Register;
begin
  {$I faspeedbutton_icon.lrs}
  RegisterComponents('FontAwesome',[TFASpeedButton]);
end;

{ TFASpeedButton }


procedure TFASpeedButton.SetFontAwesomeColor(AValue: TColor);
begin
  if FFontAwesomeColor=AValue then Exit;
  FFontAwesomeColor:=AValue;
  SetIconFa;
end;

procedure TFASpeedButton.SetFontAwesomeIcon(AValue: String);
begin
  if FFontAwesomeIcon=AValue then Exit;
  FFontAwesomeIcon:=AValue;
  SetIconFa;
end;


procedure TFASpeedButton.SetFontAwesomePath(AValue: String);
begin
  if FFontAesomewPath=AValue then Exit;
  FFontAesomewPath:=AValue;
  SetIconFa;
end;

procedure TFASpeedButton.SetFontAwesomeSize(AValue: Integer);
begin
  if FFontAwesomeSize=AValue then Exit;
  FFontAwesomeSize:=AValue;
  SetIconFa;
end;

procedure TFASpeedButton.SetIconFa;
var b: TBitmap;
begin
  b := CreateFAIcon(FontAwesomeIcon,{$IFDEF Linux}FFontAesomewPath,{$ENDIF}FFontAwesomeColor,FFontAwesomeSize);
    try
     Glyph.Assign(b);
    finally
      b.Free;
    end;
end;

constructor TFASpeedButton.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  {$IFDEF Linux}
  FFontAesomewPath:= '/usr/share/fonts/truetype/fontAwesome.ttf';
  {$ENDIF}
  FFontAwesomeColor:=clMenuText;
  FFontAwesomeSize:=16;
  {$Warnings Off}
  FontAwesomeIcon:=#61610;
  {$Warnings on}
end;



end.
