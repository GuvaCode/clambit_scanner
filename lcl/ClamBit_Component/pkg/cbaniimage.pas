unit CBAniImage;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, LResources, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  uanimationcontrol, uanimationbasic;

type

  { TCBAniImage }

  TCBAniImage = class(TImage)
  private
   AnimationQueue: TAnimationQueue;
   AnimationTimer:TTimer;
   FMarquee: Boolean;
   procedure AniTimer(Sender: TObject);
   procedure SetMarquee(AValue: Boolean);
  protected
    { Protected declarations }
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  published
    property Marquee :Boolean read FMarquee write SetMarquee;
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('ClamBit',[TCBAniImage]);
end;

{ TCBAniImage }

procedure TCBAniImage.AniTimer(Sender: TObject);
begin
  AnimationQueue.Animate;
end;

procedure TCBAniImage.SetMarquee(AValue: Boolean);
var
  lMarqueeBackground: TAnimationControlTranslate;
  lList: TFPList;
begin
  if FMarquee=AValue then Exit;
  FMarquee:=AValue;
  ////////////////////////////
  lList:=AnimationQueue.FindGroupID(Self);
  try
    if lList.Count=0 then exit;
    lMarqueeBackground:=TObject(lList[0]) as TAnimationControlTranslate;
    if FMarquee then begin
      if lMarqueeBackground.State=eAnimationStatePaused then begin
        lMarqueeBackground.Pause;
      end else begin
        lMarqueeBackground.Start;
      end;
    end else begin
      lMarqueeBackground.Pause;
    end;
  finally
    lList.Free;
  end;
end;

constructor TCBAniImage.Create(AOwner: TComponent);
var
  MarqueeBackground: TAnimationControlTranslate;
begin
  inherited Create(AOwner);
  AnimationTimer:=TTimer.Create(self);
  AnimationTimer.Interval:=15;
  AnimationTimer.OnTimer:=@AniTimer;
  AnimationTimer.Enabled:=true;
  AnimationQueue:=TAnimationQueue.Create;
  AnimationQueue.Start(true);
   MarqueeBackground:=TAnimationControlTranslate.Create(self);
  MarqueeBackground.Duration:=600;//Trunc(Width*10);
  MarqueeBackground.AnimationOnEndAction:=eAnimationOnEndFree;
  MarqueeBackground.AutoReverse:=false;
  MarqueeBackground.Repeats:=0;
  MarqueeBackground.SetInitialPosition(Point(self.Width*-1,0));
  MarqueeBackground.SetFinalPosition(Point(Width,0));
  MarqueeBackground.AnchorLocation:=eAnimationAnchorLocationCenter;
  MarqueeBackground.FreeWithQueue:=true;
  MarqueeBackground.TransitionMode:=eAnimationTransitionLinear;
  AnimationQueue.Add(MarqueeBackground);
  MarqueeBackground.Start;
end;

destructor TCBAniImage.Destroy;
begin
   FreeAndNil(AnimationTimer);
   FreeAndNil(AnimationQueue);
  inherited Destroy;
end;

end.
