{ This file was automatically created by Lazarus. Do not edit!
  This source is only used to compile and install the package.
 }

unit GC_Components;

interface

uses
  cb_register, FASpeedButton, fatool, CBColorButton, CBDropLabel, slider, 
  LazarusPackageIntf;

implementation

procedure Register;
begin
  RegisterUnit('cb_register', @cb_register.Register);
  RegisterUnit('FASpeedButton', @FASpeedButton.Register);
  RegisterUnit('CBColorButton', @CBColorButton.Register);
  RegisterUnit('CBDropLabel', @CBDropLabel.Register);
  RegisterUnit('slider', @slider.Register);
end;

initialization
  RegisterPackage('GC_Components', @Register);
end.
