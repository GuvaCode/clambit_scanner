unit CBnotify;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, LResources, Forms, Controls, Graphics, Dialogs, Process;

type

  { TNotify }

  TCBNotify = class(TComponent)
  private
    FIconName: String;
    FMessage: String;
    FPlaySound: boolean;
    FShowTime: integer;
    FSoundPath: String;
    FTitle: String;

    procedure SetIconName(AValue: String);
    procedure SetMessage(AValue: String);
    procedure SetPlaySound(AValue: boolean);
    procedure SetShowTime(AValue: integer);
    procedure SetSoundPath(AValue: String);
    procedure SetTitle(AValue: String);

    { Private declarations }
  protected
    { Protected declarations }
  public
    procedure NotifySend;
  published
    property Title:String read FTitle write SetTitle;//Заголовок
    property Message:String read FMessage write SetMessage;//Текст сообщения
    property IconName:String read FIconName write SetIconName;//Имя стоковой иконки
    property ShowTime:integer read FShowTime write SetShowTime default 0;//Время показа сообщения в милисекундах 0 по умолчанию
    property PlaySound:boolean read FPlaySound write SetPlaySound default false;//Проигровать звук используя Play
    property SoundPath:String read FSoundPath write SetSoundPath;//путь для звука


  end;


implementation
uses LazFileUtils,fileutil;

{ TCBNotify }

procedure TCBNotify.SetIconName(AValue: String);
begin
  if FIconName=AValue then Exit;
  FIconName:=AValue;
end;

procedure TCBNotify.SetMessage(AValue: String);
begin
  if FMessage=AValue then Exit;
  FMessage:=AValue;
end;

procedure TCBNotify.SetPlaySound(AValue: boolean);
begin
  if FPlaySound=AValue then Exit;
  FPlaySound:=AValue;
end;

procedure TCBNotify.SetShowTime(AValue: integer);
begin
  if FShowTime=AValue then Exit;
  FShowTime:=AValue;
end;

procedure TCBNotify.SetSoundPath(AValue: String);
begin
  if FSoundPath=AValue then Exit;
  FSoundPath:=AValue;
end;

procedure TCBNotify.SetTitle(AValue: String);
begin
  if FTitle=AValue then Exit;
  FTitle:=AValue;
end;

procedure TCBNotify.NotifySend;
var shProcess,PlProcess: TProcess;
begin
 shProcess := TProcess.Create(nil);
 shProcess.Executable := 'notify-send';
 If FIconName <> '' then
 begin
 shProcess.Parameters.Add('-i');
 shProcess.Parameters.Add(FIconName);
 end;
 If FShowTime > 0 then
 begin
 shProcess.Parameters.Add('-t');
 shProcess.Parameters.Add(IntToStr(FShowTime));
 end;
 shProcess.Parameters.Add(FTitle);
 shProcess.Parameters.Add(FMessage);

 //Play Sound
  if (PlaySound) and (FindDefaultExecutablePath('play') <> '') and (FileExists(FSoundPath)) then
  begin
  plProcess := TProcess.Create(nil);
  plProcess.Executable := 'play';
  plProcess.Parameters.Add(FSoundPath);
  plProcess.Execute;
  plProcess.Free;
  end;
 //Show Message
 shProcess.Execute;
 shProcess.Free;
end;


//  if (FindDefaultExecutablePath('play') <> '') then
end.
