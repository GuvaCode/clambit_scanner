unit munit;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  GCNotify;

type

  { TForm1 }

  TForm1 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    GCNotify1: TGCNotify;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.Button1Click(Sender: TObject);
begin

  with GCNotify1 do
   begin
     Title:='Test Notification Components';
     Message:='Like and fast notification on desktop';
     IconName:='help-about';
     NotifySend;
   end;

end;

procedure TForm1.Button2Click(Sender: TObject);
begin
    with GCNotify1 do
   begin
     Title:='Test Notification Components';
     Message:='Show and play sound (uses Play application)';
     IconName:='media-optical-audio-new';
     PlaySound:=true;
     SoundPath:='/usr/share/sounds/freedesktop/stereo/complete.oga';
     NotifySend;
     PlaySound:=false;
   end;
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
   with GCNotify1 do
   begin
     Title:='Test Notification Components';
     Message:='<b>HTML</b> <i>Style</i> <u>Message</u>';
    // IconName:='weather-storm';  //connect.png
      IconName:=GetCurrentDir+'/connect.png';  //connect.png
     NotifySend;
   end;
end;

end.

